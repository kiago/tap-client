function loadPage() {
    generateTopMenu();
    if (!isEmpty(getUsername())) {
        showMainContainer();
    }
    loadTable(1); // demo data jaanuar
}

// function loadTable(int = new Date().getMonth() + 1) {
function loadTable(int = 1) { // demo data jaanuar
    showTableView(int);
    generateMonthFilter(int);
    fetchMonth(int).then(response => generateTableHead(response));
    loadWorkingDays(int);

}

function loadUsers() {
    fetchUsers().then(showUsersView);
}

function loadWorkingDays(int) {
    fetchWorkingDaysTable(int).then(data => { 
        generateTableRows(data)
        generateHeaderSummary(data);
    });
}

function handleDeleteButton(id) {
    if (confirm("Oled sa kindel, et soovid kustutada?")){
        deleteUserById(id).then(loadUsers);
        messageToMainContainer('danger', `Töötaja <id: ${id}> konto eemaldatud`);
    }
}

function handleEditButton(id) {
    openUserModal();
    fetchUser(id).then(
        function(user) {
            document.getElementById("id").value = user.id,
            document.getElementById("brigadeID").value = user.brigadeID,
            document.getElementById("squadNumber").value = user.squadNumber,
            document.getElementById("firstName").value = user.firstName,
            document.getElementById("lastName").value = user.lastName,
            document.getElementById("idCode").value = user.idCode,
            document.getElementById("phone").value = user.phone,
            document.getElementById("email").value = user.email,
            document.getElementById("address").value = user.address,
            document.getElementById(user.roleName).checked = true,
            document.getElementById("userCompetences").value = user.userCompetences;
        }
    )
}

function handleAddButton(user) {
    openUserModal();
    document.getElementById("id").value = null,
    document.getElementById("brigadeID").value = null,
    document.getElementById("squadNumber").value = null,
    document.getElementById("firstName").value = null,
    document.getElementById("lastName").value = null,
    document.getElementById("idCode").value = null,
    document.getElementById("phone").value = null,
    document.getElementById("email").value = null,
    document.getElementById("address").value = null,
    document.getElementsByName('roleName').forEach(r => { r.checked = false; }),
    document.getElementById("userCompetences").value = null
}

function handleSave() {
    if (isFormValid() === false) {
        return;
    }
    if (document.getElementById("id").value > 0) {
        handleEdit();
    } else {
        handleAdd();
    }
}

function handleEdit() {
    let user = {
        id: document.getElementById("id").value,
        brigadeID: document.getElementById("brigadeID").value,
        squadNumber: document.getElementById("squadNumber").value,
        firstName: document.getElementById("firstName").value,
        lastName: document.getElementById("lastName").value,
        idCode: document.getElementById("idCode").value,
        phone: document.getElementById("phone").value,
        email: document.getElementById("email").value,
        address: document.getElementById("address").value,
        roleName: document.querySelector('input[name="roleName"]:checked').value,
        userCompetences: document.getElementById("userCompetences").value
    }

    putUser(user).then(
        function() {
            loadUsers();
            closeUserModal();
            messageToMainContainer('success', `Töötaja ${user.firstName} ${user.lastName} andmed muudetud`);
        }
    )
}

function handleAdd() {
    let user = {
        brigadeID: document.getElementById("brigadeID").value,
        squadNumber: document.getElementById("squadNumber").value,
        firstName: document.getElementById("firstName").value,
        lastName: document.getElementById("lastName").value,
        idCode: document.getElementById("idCode").value,
        phone: document.getElementById("phone").value,
        email: document.getElementById("email").value,
        address: document.getElementById("address").value,
        roleName: document.querySelector('input[name="roleName"]:checked').value,
        userCompetences: document.getElementById("userCompetences").value
    };

    postUser(user).then(
        function() {
            loadUsers();
            closeUserModal();
            messageToMainContainer('success', `Lisatud uus töötaja: ${user.firstName} ${user.lastName}`);
        }
    );
}

function importFile() {
    document.getElementById('fileInput').click();
    document.getElementById('fileInput').onchange = function() {
        importFilePath(document.getElementById('fileInput').files[0])
        .then(filepath => importFileData(filepath));
    }
}

function isFormValid() {
    let brigadeID = document.getElementById("brigadeID").value;
    let squadNumber = document.getElementById("squadNumber").value;
    let firstName = document.getElementById("firstName").value;
    let lastName = document.getElementById("lastName").value;
    let idCode = document.getElementById("idCode").value;
    let phone = document.getElementById("phone").value;
    let email = document.getElementById("email").value;
    let address = document.getElementById("address").value;
    let roleName = document.querySelector('input[name="roleName"]:checked').value;

    if (brigadeID === null || brigadeID.length < 1) {
        document.getElementById("errorMessage").innerText = "Komando sisestamine on kohustuslik"
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (squadNumber === null || squadNumber.length < 1) {
        document.getElementById("errorMessage").innerText = "Valvevahetuse sisestamine on kohustuslik"
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (firstName === null || firstName.length < 1) {
        document.getElementById("errorMessage").innerText = "Töötaja eesnimi on kohustuslik"
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (lastName === null || lastName.length < 1) {
        document.getElementById("errorMessage").innerText = "Töötaja perekonnanimi on kohustuslik"
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (idCode === null || idCode.length < 1) {
        document.getElementById("errorMessage").innerText = "Isikukood on kohustuslik"
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (phone === null || phone.length < 1) {
        document.getElementById("errorMessage").innerText = "Telefoninumber on kohustuslik"
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (email === null || email.length < 1) {
        document.getElementById("errorMessage").innerText = "Kontakt e-mail aadress on kohustuslik"
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (address === null || address.length < 1) {
        document.getElementById("errorMessage").innerText = "Elukoha aadress on kohustuslik"
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (roleName === null || roleName.length < 1) {
        document.getElementById("errorMessage").innerText = "Ametikoht on kohustuslik"
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    document.getElementById("errorMessage").style.display = "none";
    return true;
}

function loginUser() {
    let credentials = getCredentialsFromLoginContainer();
    if (validateCredentials(credentials)) {
        login(credentials).then(session => {
            storeAuthentication(session);
            generateTopMenu();
            loadPage();
        })
    }
}

function logoutUser() {
    clearAuthentication();
    generateTopMenu();
    showLoginContainer();
}
