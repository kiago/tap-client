function fetchUsers() {
    return fetch( `${API_URL}/users`, { 
        method: 'GET',
        headers: { 'Authorization': `Bearer ${getToken()}` } 
    } )
    .then(response => response.json());
}

function fetchUser(id) {
    return fetch( `${API_URL}/user?id=${id}`, { 
        method: 'GET',
        headers: { 'Authorization': `Bearer ${getToken()}` } 
    } )
    .then(response => response.json());
}

function postUser(user) {
    return fetch(`${API_URL}/user`, { 
        method: 'POST', 
        headers: { "Content-Type": "application/json", 'Authorization': `Bearer ${getToken()}` }, 
        body: JSON.stringify(user) 
    } );
}

function putUser(user) {
    return fetch(`${API_URL}/user`, { 
        method: 'PUT', 
        headers: { "Content-Type": "application/json", 'Authorization': `Bearer ${getToken()}` },
        body: JSON.stringify(user) 
    } );
}

function deleteUserById(id) {
    return fetch( `${API_URL}/user?id=${id}`, { 
        method: 'DELETE',
        headers: { 'Authorization': `Bearer ${getToken()}` }
    });
}

function login(credentials) {
    return fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
    .then(checkResponse).then(session => session.json());
}

function checkResponse(response) {
    if (!response.ok) {
        clearAuthentication();
        showLoginContainer();
        closeUserModal();
        generateTopMenu();
        throw new Error(response.status);
    }
    showMainContainer();
    return response;
}

function uploadFile(file) {
    let formData = new FormData();
    formData.append("file", file);

    return fetch(
            `${API_URL}/files/upload`, 
            {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${getToken()}`
                },
                body: formData
            }
    )
    .then(checkResponse).then(response => response.json());
}

function importFilePath(file) {
    let formData = new FormData();
    formData.append("file", file);

    return fetch(
            `${API_URL}/files/filepath`, 
            {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${getToken()}`
                },
                body: formData
            }
    )
    .then(checkResponse).then(response => response.text());
}

function importFileData(fileFullPath) {
    let formData = new FormData();
    formData.append("path", fileFullPath);
    return fetch(
            `${API_URL}/files/import`, 
            {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${getToken()}`
                },
                body: formData
            }
    )
}

function fetchDayStatus(date) {
    return fetch(
        `${API_URL}/table/dayStatus?d=${date}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            },
        }
    )
    .then(checkResponse).then(response => response.text());
}

function fetchMonth(monthNumValue) {
    return fetch(
        `${API_URL}/table/month?m=${monthNumValue}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            },
        }
    )
    .then(checkResponse).then(response => response.json());
}

// Returns List of public holidays // raw json from api 
function getHolidays(day) {
    let formData = new FormData();
    formData.append("path", fileFullPath);
    return fetch(
        `${API_URL}/day/holiday`, {
            method: 'GET',
            headers: {
                'Content-Type': 'text/plain', 'Authorization': `Bearer ${getToken()}`
            },
            body: formData
        }
    );
}

function fetchWorkingDaysTable(monthInt) {
    return fetch( `${API_URL}/table/workdays?m=${monthInt}`, { 
        method: 'GET',
        headers: { 'Authorization': `Bearer ${getToken()}` } 
    })
    .then(response => response.json());
}

function fetchDogImage() {
    return fetch( `https://dog.ceo/api/breeds/image/random`, { 
        method: 'GET'
    } )
    .then(response => response.json());
}