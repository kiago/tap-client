function generateTopMenu() {
    let managementMenu = "";
    if (!isEmpty(getUsername())) {
        managementMenu = `
        <div class="col-2 tab" id="userTab" onClick="loadUsers()">Töötajad</div>
        <div class="col-2 tab tab-active" id="tableTab" onClick="loadTable()">Töögraafik</div>
        <div class="col-2 ml-auto" style="text-align: right;">${getUsername()} | <a href="javascript:logoutUser()">Logi välja</a></div>`;
    }
    document.getElementById("topMenuContainer").innerHTML = `
        <div class="topMenuHead">
            <div class="row">
                <div class="col-2 mr-auto"><img src="/assets/rescue.png" height=60px alt="Logo" id="logo" onclick="loadPage()"/></div>
                <!-- <div class="inline col-2 mr-auto">TÖÖGRAAFIKUD</div> -->
                ${managementMenu}
            </div>
        </div>`;
}

// async function showUsersView(inputResponse) {
function showUsersView(inputResponse) {
    let usersHtml = `
    <div class="row justify-content-center">
        <div class="">
        <button type="button" class="btn btn-success" onclick="handleAddButton()">Lisa uus töötaja</button>         
        <button type="button" class="btn btn-success" onclick="importFile()">Lisa failist</button>
        <input id="fileInput" type="file" name="hiddenfileInput" style="display: none;" />
        </div>
    </div>
    <div class="row justify-content-center">`;
    for (let i = 0; i < inputResponse.length; i++) {
        // let image = await fetchDogImage();
        // let image = { message: 'https://i.pinimg.com/236x/04/02/d5/0402d586675b63244b401cb8485d2bdc--hot-firemen-hot-firefighters.jpg', status:"200" }
        let image = { message: 'http://www.clker.com/cliparts/d/L/P/X/z/i/no-image-icon-md.png', status:"200" }
        let userHtml = "";
        const user = inputResponse[i];
        if (user.firstName != "Admin") {
            userHtml = userHtml + `
                <div id="${user.id}" class="row user-card">
                    <div class="col">
                        <h5>${user.firstName + " " + user.lastName}</h5>
                        <p>${user.roleName}</p>
                        <p>Pädevused: N/A </p>
                        <img src="${image.message}" alt="${user.firstName + user.lastName}">
                    </div>
                    <div id="userInfo" class="col">
                        <p>${user.brigadeName + " komando"}</p>
                        <p>ik: ${user.idCode}</p>
                        <p>tel: ${user.phone}</p>
                        <p>${user.email}</p>
                        <p>${user.address}</p>
                        <button class="btn btn-primary" onclick="handleEditButton(${user.id})">Muuda</button>
                        <button class="btn btn-danger" onclick="handleDeleteButton(${user.id})">Kustuta</button>
                    </div>
                </div>`
            usersHtml = usersHtml + userHtml;
        }
    }
    document.getElementById('content').innerHTML = usersHtml + `</div>`
}

function showTableView(int) {
    let month = getMonthFilterEE(int-1);
    let tableHtml = `
    <div class="row justify-content-center">
        <div class="row main-header">
            <div class="btn-group ml-auto" role="group" style="">
                <button type="button" class="btn btn-primary">I</button>
                <button type="button" class="btn btn-primary">II</button>
                <button type="button" class="btn btn-primary">III</button>
                <button type="button" class="btn btn-primary">IV</button>
            </div>
            <div>
                <button type="button" class="btn btn-success" disabled onclick="importFile()">Lisa uus graafik failist</button>
                <input id="fileInput" type="file" name="hiddenfileInput" style="display: none;" />
            </div>
            <div class="btn-group">
                <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="currentMonth" name="${month.hours}">${month.name} (${month.hours})</button>
                <ul id="monthList" class="dropdown-menu"></ul>
            </div>
        </div>
    </div>
    <div class="row justify-content-center" id="workTable">
        <table id="calendarView" class="table table-responsive">
            <thead>
                <tr id="tableHead"></tr>
                <tr id="summary" style="background: #eee;"></tr>
            </thead>
        </table>
    </div>`
    document.getElementById('content').innerHTML = tableHtml;
}

function generateMonthFilter(int) {
    let listItems = "";
    for (let i = -5; i < 5; i++) {
        let monthNumber = int + i; 

        let month = getMonthFilterEE(monthNumber);

        if (i == -1) { // praegune kuu
            listItems = listItems + `<li onclick="loadTable(${month.num})"><strong>${month.name}</strong></li>`;
        } else {
            listItems = listItems + `<li onclick="loadTable(${month.num})">${month.name}</li>`
        }
    }
    document.getElementById("monthList").innerHTML = listItems;
}

function showLoginContainer() {
    document.getElementById("loginContainer").style.display = "block";
    document.getElementById("mainContainer").style.display = "none";
}

function showMainContainer() {
    document.getElementById("loginContainer").style.display = "none";
    document.getElementById("mainContainer").style.display = "block";
}

function closeUserModal() { $("#userModal").modal("hide"); }

function openUserModal() { $("#userModal").modal("show"); }

function messageToMainContainer(type, message) {
    let newAlert = document.getElementById("mainContainerAlert");
    switch(type) {
        case 'success':
            newAlert.classList.add('alert-success');
            break;
        case 'warning':
            newAlert.classList.add('alert-warning');
            break;
        case 'primary':
            newAlert.classList.add('alert-primary');
            break;
        case 'danger':
            newAlert.classList.add('alert-danger');
            break;
    }
    newAlert.innerText = message;
    newAlert.style.display = "block";
}

async function generateTableHead(month) {  
    document.getElementById('tableHead').innerHTML = document.getElementById('tableHead').innerHTML + `<th><input type="text" class="form-control input-sm" id="filterNames" placeholder="Filtreeri..."/></th><th>A</th>`;
    month.days.forEach(day => {
        createTableHeadDay(day);
    });
    document.getElementById('tableHead').innerHTML = document.getElementById('tableHead').innerHTML + `<th>SUM</th>`;
}

function createTableHeadDay(day) {
    let theadHtml = document.getElementById('tableHead').innerHTML;
    document.getElementById('tableHead').innerHTML = `${theadHtml}<th title="${day.hTitle}" ${styleInt(day.hHolidayId)}${day.id.substring(8,10)}</th>`;
}

function styleInt(int) {
    if (int == 1 || int == 2) { // riigipüha rahvuspüha
        return ` class="rp">`; } 
    else if (int == 3) { // riiklik tähtpäev
        return ` class="rt">`; }
    else if (int == 4) { // lühendatud tööpäev
        return ` class="lt">`; }
    else if (int == 5) { // nädalavahetus
        return ` class="nv">`; }
    else { // tavaline päev 
        return `>`; }
}

function generateTableRows(data) {
    let tableHtml = document.getElementById("calendarView").innerHTML;

    for (let j = 0; j < data.length; j++) {
        let userRow = data[j];
        let newRow = `<tbody><tr><th id="${userRow.id}" style="text-align: left;">${userRow.firstName + " " + userRow.lastName}</th><td>${userRow.roleSymbol}</td>`;
        let summaryCol = 0;
        for (let i = 4; i < Object.keys(userRow).length; i++) { // andmebaasi disainis on manuaalne 31 päeva, laadimisel loob seega 31+ veergu
            let cellValue = userRow[Object.keys(userRow)[i]];
            summaryCol += cellValue > 0 ? parseInt(cellValue) : 0;
            newRow = newRow + `<td>${cellValue}</td>`
        }
        let sumStyle = "";
        let normHours = document.getElementById('currentMonth').name;
        if(summaryCol > parseInt(normHours)) {sumStyle = ` class="overRed" title="Normtunnid ületatud"`};
        tableHtml = tableHtml + newRow + `<th id="test"${sumStyle}>${summaryCol}</th></tr></tbody>`;
    }
    document.getElementById("calendarView").innerHTML = tableHtml;
}

function generateHeaderSummary(data) {
    let summaryRow = '<th colspan="2" style="text-align: left;">KOKKU tööl</th>';
    for (let day = 1; day < 32; day++) { // length == 1(id) + 31
        let workersCount = 0;
        let sum = calculateSummaryHoursForDay(data, day);
        sum.forEach(worker => { worker > 0 ? workersCount++ : ''});       
        summaryRow = summaryRow + `<td title="Sel päeval tööl kokku">${workersCount}</td>`
    }
    document.getElementById("summary").innerHTML = summaryRow + `</tr>`
}

function calculateSummaryHoursForDay(data, dayNumber) {
    dayNumber = dayNumber < 10 ? 'd0' + dayNumber : 'd' + dayNumber;
    return data.map(p => parseInt(p[dayNumber]) > 0 ? parseInt(p[dayNumber]) : 0);
}