function validateCredentials(credentials) {
    return !isEmpty(credentials.username) && !isEmpty(credentials.password);
}

function getCredentialsFromLoginContainer() {
    return {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value
    };
}

function isEmpty(text) {
    return (!text || 0 === text.length);
}

function generateImageElement(image) {
    if (!isEmpty(image)) {
        return `<img src="${image}" width="150" />`;
    } else {
        return "[PILT PUUDUB]";
    }
}

function getMonthFilterEE(int) {
    var months = [ {name:"jaanuar", num: 1, hours: 176 }, {name:"veebruar", num: 2, hours: 160 }, {name:"märts", num: 3, hours: 168 }, {name:"aprill", num: 4, hours: 168 }, 
        {name:"mai", num: 5, hours: 176 }, {name:"juuni", num: 6, hours: 152 }, {name:"juuli", num: 7, hours: 184 }, {name:"august", num: 8, hours: 168 }, {name:"september", num: 9, hours: 168 }, 
        {name:"oktoober", num: 10, hours: 184 }, {name:"november", num: 11, hours: 168 }, {name:"detsember", num: 12, hours: 146 } ];
    
    int = int < 0 ? int + 12 : int;
    int = int > 12 ? int % 12 : int;
    
    return months[int];
}

$(document).on("click", 'div.tab', function(){
    $('div.tab').removeClass('tab-active');
    $(this).addClass('tab-active');
});

// table highlighting
$("body").addClass("nohover");
$("td, th").attr("tabindex", "1").on("touchstart", () => $(this).focus());

// $("#filterNames").keydown(function(e) {alert('alert!')});

$("#loginContainer").keyup(function(e){ 
    var code = e.which;
    if (code == 13) e.preventDefault();
    if (code == 32 || code == 13 || code == 188 || code == 186) { loginUser(); } 
});

$(document).on('click', 'td', function() {
    alert("Uus muutmise aken.")
});
