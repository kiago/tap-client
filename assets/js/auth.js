
function clearAuthentication() {
    localStorage.removeItem(TAP_AUTH_TOKEN);
    localStorage.removeItem(TAP_AUTH_USERNAME);
}

function storeAuthentication(session) {
    localStorage.setItem(TAP_AUTH_TOKEN, session.token);
    localStorage.setItem(TAP_AUTH_USERNAME, session.username);
}

function getUsername() {
    return localStorage.getItem(TAP_AUTH_USERNAME);
}

function getToken() {
    return localStorage.getItem(TAP_AUTH_TOKEN);
}
